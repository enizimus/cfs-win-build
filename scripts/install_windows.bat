REM SPDX-FileCopyrightText: 2022 Intel Corporation
REM
REM SPDX-License-Identifier: MIT

set ONEAPI_BASE_URL=%1
set ONEAPI_HPC_URL=%2
set UTILS_URL=%3
set CMAKE_VERSION=%4
set JOM_VERSION=%5

echo Installing CMAKE, PYTHON and JOM

@REM Install cmake and python :
choco install --confirm cmake.install --version=%CMAKE_VERSION%
choco install --confirm python
choco install --confirm jom --version=%JOM_VERSION%

echo Downloading ONEAPI and UNIXUTILS 

curl.exe --output %TEMP%\oneapi_base.exe --url %ONEAPI_BASE_URL% --retry 5 --retry-delay 5
curl.exe --output %TEMP%\oneapi_hpc.exe --url %ONEAPI_HPC_URL% --retry 5 --retry-delay 5
curl.exe -L --output %TEMP%\UnixUtils.msi --url %UTILS_URL% --retry 5 --retry-delay 5

start /b /wait %TEMP%\oneapi_base.exe -s -x -f oneapi_base_extracted --log oneapi_base_extract.log
start /b /wait %TEMP%\oneapi_hpc.exe -s -x -f oneapi_hpc_extracted --log oneapi_hpc_extract.log
start /b /wait %TEMP%\UnixUtils.msi /passive

echo Deleting reudundant files

del %TEMP%\oneapi_base.exe
del %TEMP%\oneapi_hpc.exe
del %TEMP%\UnixUtils.msi

echo Running ONEAPI installation

oneapi_base_extracted\bootstrapper.exe -s --action install --components=intel.oneapi.win.mkl.devel --eula=accept -p=NEED_VS2017_INTEGRATION=0 -p=NEED_VS2019_INTEGRATION=0 -p=NEED_VS2022_INTEGRATION=0 --log-dir=.
oneapi_base_extracted\bootstrapper.exe -s --action install --components=intel.oneapi.win.tbb.devel --eula=accept -p=NEED_VS2017_INTEGRATION=0 -p=NEED_VS2019_INTEGRATION=0 -p=NEED_VS2022_INTEGRATION=0 --log-dir=.
oneapi_base_extracted\bootstrapper.exe -s --action install --components=intel.oneapi.win.dpcpp_debugger --eula=accept -p=NEED_VS2017_INTEGRATION=0 -p=NEED_VS2019_INTEGRATION=0 -p=NEED_VS2022_INTEGRATION=0 --log-dir=.

oneapi_hpc_extracted\bootstrapper.exe -s --action install --components=intel.oneapi.win.cpp-compiler --eula=accept -p=NEED_VS2017_INTEGRATION=0 -p=NEED_VS2019_INTEGRATION=0 -p=NEED_VS2022_INTEGRATION=0 --log-dir=.
oneapi_hpc_extracted\bootstrapper.exe -s --action install --components=intel.oneapi.win.ifort-compiler --eula=accept -p=NEED_VS2017_INTEGRATION=0 -p=NEED_VS2019_INTEGRATION=0 -p=NEED_VS2022_INTEGRATION=0 --log-dir=.

set installer_exit_code=%ERRORLEVEL%
rd /s/q "oneapi_base_extracted"
rd /s/q "oneapi_hpc_extracted"
exit /b %installer_exit_code%