set /A N_CORES_JOM=$1
set VS_VER=$2
set SAMPLES_TAG=$3
set DEV_ENV=$4

echo "Setting up build requirements :"
 
SET PATH=%PATH%;C:\Program Files\CMake\bin
SET INTEL_DIR=C:\Program Files (x86)\Intel\oneApi
SET CFS_ROOT_DIR=C:\Users\pajser\Projects\cfs\
SET PATH=%PATH%;%ProgramFiles(x86)%\UnixUtils\usr\local\wbin
SET MKL_ROOT_DIR=C:\Program Files (x86)\Intel\oneAPI\mkl\%SAMPLES_TAG%

IF "%VS_VER%"=="2017_build_tools" (
    @call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
) ELSE (
    IF "%VS_VER%"=="2019_build_tools" (
        @call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
    ) ELSE (
          @call "C:\Program Files (x86)\Intel\oneAPI\setvars-vcvarsall.bat" %VS_VER%
    )
)

for /f "tokens=* usebackq" %%f in (`dir /b "C:\Program Files (x86)\Intel\oneAPI\compiler\" ^| findstr /V latest ^| sort`) do @set "LATEST_VERSION=%%f"
@call "C:\Program Files (x86)\Intel\oneAPI\compiler\%LATEST_VERSION%\env\vars.bat"

@REM set correct compilers : 
set CC=icl
set CXX=icl
set FC=ifort

@REM make directories : 
mkdir CFS_build\
mkdir CFS_build\nmake

@REM run cmake : 
@REM cmake -DBUILD_TESTING=ON -G "NMake Makefiles JOM" -S .\ -B CFS_build\nmake\
cmake -G "NMake Makefiles JOM" -S .\ -B CFS_build\nmake\

@REM build cfs for windows : 
cd CFS_build\nmake\
jom /S /A /J 6 /f Makefile

@REM clean and zip the build directory :
rmdir /q /s cfsdeps
rmdir /q /s source 
rmdir /q /s CMakeFiles 
rmdir /q /s include
rmdir /q /s cmake
rmdir /q /s tmp
del /Q *.cmake *.txt Makefile

cd .. 
zip -r CFS_win_build.zip nmake
rmdir /q /s nmake

echo "Finished building CFS!"