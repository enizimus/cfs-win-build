set VS_VER=2019_build_tools
set SAMPLES_TAG=2021.3.0
set DEV_ENV=vs2019

echo "Setting up build requirements :"
 
SET PATH=%PATH%;C:\Program Files\CMake\bin
SET INTEL_DIR=C:\Program Files (x86)\Intel\oneApi
SET CFS_ROOT_DIR=C:\Users\pajser\Projects\cfs\
SET PATH=%PATH%;%ProgramFiles(x86)%\UnixUtils\usr\local\wbin
SET MKL_ROOT_DIR=C:\Program Files (x86)\Intel\oneAPI\mkl\%SAMPLES_TAG%

IF "%VS_VER%"=="2017_build_tools" (
    @call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
) ELSE (
    IF "%VS_VER%"=="2019_build_tools" (
        @call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
    ) ELSE (
          @call "C:\Program Files (x86)\Intel\oneAPI\setvars-vcvarsall.bat" %VS_VER%
    )
)

for /f "tokens=* usebackq" %%f in (`dir /b "C:\Program Files (x86)\Intel\oneAPI\compiler\" ^| findstr /V latest ^| sort`) do @set "LATEST_VERSION=%%f"
@call "C:\Program Files (x86)\Intel\oneAPI\compiler\%LATEST_VERSION%\env\vars.bat"

echo "Checking build requirements :"

@REM set correct compilers : 
set CC=icl
set CXX=icl
set FC=ifort

@REM make directories : 
mkdir CFS_build\
mkdir CFS_build\nmake

@REM show software versions : 
icl /QV
ifort /QV
cmake --version
python --version
ls --version
jom --version 

echo "Finished testing space!"