# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
set(gidpost_source  "@gidpost_source@")
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
SET(CMAKE_CXX_COMPILER_ID "@CMAKE_CXX_COMPILER_ID@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")

SET(files
  "source/CMakeLists.txt"
  "source/gidpost.c"
  "source/gidpostforAPI.c"
  "source/gidpostfor.c"
  "source/gidpostfor.h"
  "source/gidpost.h"
  "source/gidpostHash.c"
  "source/gidpostHash.h"
  "source/gidpostHDF5.c"
  "source/gidpostHDF5.h"
  "source/gidpostInt.c"
  "source/gidpostInt.h"
  "source/hashtab.c"
  "source/hashtab.h"
  "source/hdf5c.c"
  "source/hdf5c.h"
  "source/lookupa.c"
  "source/lookupa.h"
  "source/Makefile"
  "source/recycle.c"
  "source/recycle.h"
  "source/standard.h"
  "examples/testpost.c"
  "examples/testpost_fd.c"
  )

CHANGE_NEWLINE_STYLE("${files}" ${gidpost_source}/dummy.txt UNIX)

#=============================================================================
# Apply some patches.
#=============================================================================
SET(patches
  # Add append parameter to GiD_OpenPostResultFile for restart capability.
  "gidpost-file-append.patch"

  # Make sure GiDpost gets installed to the correct location.
  "gidpost-install.patch"

  # Make sure GiDpost can find all openCFS Fortran libs for linking its
  # Fortran example.
  "gidpost-fortran.patch"

  # Depreciated: What do we need hdf5 for??
  # Remove HDF5 lite functions from source/hdf5c.c and add dependency to
  # HDF5_LT_LIBRARY to targets in examples/CMakeLists.txt
  # "gidpost-hdf5-lite.patch"
  )

APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/gidpost")
